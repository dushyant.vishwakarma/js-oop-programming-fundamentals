// Constructor
function Book(title,author,year){
    this.title = title;
    this.author = author;
    this.year = year;
}

// prototype of function getSummary()
Book.prototype.getSummary = function() {
    return `${this.title} was written by ${this.author} in the year ${this.year}`;
}

// Inheritance
// Magazine Constructor
function Magazine(title,author,year,month){
    Book.call(this,title,author,year);   // Inheriting the properties of Book in Magazine

    this.month = month;
}

// Inheriting the prototype from Book
Magazine.prototype = Object.create(Book.prototype);

// Instantiating magazine object
const mag1 = new Magazine('Mag One','Kyle will','2020','February');

// Use magazine constructor
Magazine.prototype.constructor = Magazine;

console.log(mag1);
console.log(mag1.getSummary());
