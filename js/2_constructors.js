// Constructor
function Book(title,author,year){
    this.title = title;
    this.author = author;
    this.year = year;

    this.getSummary = () => {
        return `${this.title} was written by ${this.author} in the year ${this.year}`;
    }
}

// Instantiate any number of objects
const book1 = new Book('Book 1','John Cena',2012);
const book2 = new Book('Book 2','Snoop Dogg',2015);

console.log(book1);
console.log(book2);
