// Object Protos
const bookProtos = {
    getSummary: function(){
        return `${this.title} was written by ${this.author} in the year ${this.year}`;
    },
    getAge: function(){
        const years = new Date().getFullYear() - this.year;
        return `${this.title} is ${years} years old`;
    }
}

// Create Object
// const book1 = Object.create(bookProtos);
// book1.title = 'Book Example';
// book1.author = 'Author Example';
// book1.year = '2013';

// OR 
const book1 = Object.create(bookProtos, {
    title: {value: 'Book 1'},
    author: {value: 'Author 1'},
    year: {value: '2014'},
});

console.log(book1);