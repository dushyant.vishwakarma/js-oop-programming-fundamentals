// Constructor
function Book(title,author,year){
    this.title = title;
    this.author = author;
    this.year = year;
}

// prototype of function getSummary()
Book.prototype.getSummary = function() {
    return `${this.title} was written by ${this.author} in the year ${this.year}`;
}

// prototype of function getAge() to get age of the book
Book.prototype.getAge = function() {
    const years = new Date().getFullYear() - this.year;
    return `${this.title} is ${years} years old`;
}

// Revise book / change year
Book.prototype.revise = function(newYear) {
    this.year = newYear;
    this.revised = true;
}


// Instantiate any number of objects
const book1 = new Book('Book 1','John Cena',2012);
const book2 = new Book('Book 2','Snoop Dogg',2015);

// Use Prototype
// console.log(book1.getSummary());

// console.log(book1.getSummary());
// console.log(book2);

// console.log(book1.getAge());

console.log(book1);
book1.revise('2017');
console.log(book1);

