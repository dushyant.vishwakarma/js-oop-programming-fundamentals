// Create Class
class Book {
    constructor(title,author,year){
        this.title = title;
        this.author = author;
        this.year = year;
    }

    getSummary(){
        return `${this.title} was written by ${this.author} in the year ${this.year}`;
    }

    getAge(){
        const years = new Date().getFullYear() - this.year;
        return `${this.title} is ${years} years old`;
    }

    revise(newYear){
        this.year = newYear;
        this.revised = true;
    }

    static topBookStore(){
        return 'Rajesh Book Center';
    }
}

// Subclass of class Book
class Magazine extends Book{
    constructor(title,author,year,month){
        // Calling parent constructor
        super(title,author,year);
        // set the parameters of this constructor
        this.month = month;
    }
}

// Instantiate the object of subclass
const mag1 = new Magazine('Mag 1','Melissa','2017','February');

console.log(mag1);

// calling the methods of parent class
console.log(mag1.getSummary());
console.log(mag1.getAge());
mag1.revise('2020');

console.log(mag1);


