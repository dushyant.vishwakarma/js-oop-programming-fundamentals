// Simple string
const str = 'This is a string';
console.log(str);

// Object String
const str2 = new String('hello');
console.log(typeof str);
console.log(typeof str2);

// The window object
console.log(window);
// window.alert('test');

// Browser Details
console.log(navigator.appVersion);

// Object Literal Example
const book1 = {
    title: 'Book 1',
    author: 'John Cena',
    year: '2015',
    getSummary: () => {
        return `${this.title} was written by ${this.author} in the year ${this.year}`;
    }
}

const book2 = {
    title: 'Book 2',
    author: 'Mary Jane',
    year: '2011',
    getSummary: () => {
        return `${this.title} was written by ${this.author} in the year ${this.year}`;
    }
}

console.log(book1);
console.log(book2);


// To get all values of object literal
console.log(Object.values(book1));
console.log(Object.values(book2));

// To get all keys of object literal
console.log(Object.keys(book1));
console.log(Object.keys(book2));


